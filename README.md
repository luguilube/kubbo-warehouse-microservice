## Kubbo Test App

Kubbo Test App es una app basada en una idea inicial de la empresa Kubbo, específicamente como prueba técnica para postulación a una vacante. Esta app fue diseñada en un principio por un conjunto de 6 microservicios para el backend  (servidor, customers, products, warehouses, orders, cloud gateway) para gestionar específicamente lo referente a cada funcionalidad requerida por la App, pero segmentada en módulos; y para el frontend un proyecto únicamente. Actualmente se sigue actualizando que nuevas funcionalidades y tecnologías para ser mostrada como proyecto de portafolio. 

## Descripción del Proyecto

Toda la descripción detallada del proyecto, se encuentra dentro del repositorio de [kubbo-test-app-frontend](https://bitbucket.org/luguilube/kubbo-test-app-frontend/src/master/) 

## Kubbo Warehouse Microservice

Microservicio que permite todas las operaciones CRUD para la entidad Almacén, a su vez permite la gestión de stock de los diferentes productos por almacén, complementando esto; también le permite al microservicio de envios, consultar disponibilidad de stock al momento de seleccionar la cantidad de productos para un envío y se vuelve a verificar justo antes de registrar una nueva orden, la cantidad de stock y si el almacén puede proveerla.

## Nota

Se recomienda que este microservicio, sea el sexto en ser ejecutado, ya que depende de la ejecución de los microservicios:.

- kubbo-config-server.
- kubbo-eureka-server.
- kubbo-product-microservice.


