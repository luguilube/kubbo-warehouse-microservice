package com.kubbo.app.warehouse.microservice.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.kubbo.app.warehouse.microservice.models.entities.Product;
import com.kubbo.app.warehouse.microservice.models.entities.ProductWarehouse;
import com.kubbo.app.warehouse.microservice.models.entities.Warehouse;
import com.kubbo.app.warehouse.microservice.models.service.WarehouseService;

@RestController
public class WarehouseController {
	private static final Logger log = LoggerFactory.getLogger(WarehouseController.class);
	private final WarehouseService service;

	@Autowired
	public WarehouseController(WarehouseService service) {
		this.service = service;
	}

	@GetMapping("/list")
	public ResponseEntity<?> list() {
		try {
			List<Warehouse> list = service.findAllByDeletedFalse().stream().map(warehouse -> {
				Set<ProductWarehouse> sList = warehouse.getProducts().stream().filter(pW -> !pW.getDeleted())
						.collect(Collectors.toSet());

				warehouse.setProducts(sList);
				
				return warehouse;
			}).collect(Collectors.toList());
			
			return ResponseEntity.ok().body(list);
		} catch (InternalError e) {
			log.error("InternalError {}", e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		} catch (Exception e1) {
			log.error("Exception {}", e1);
			return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).build();
		}
	}

	@GetMapping("/paginated-list")
	public ResponseEntity<?> paginatedList(Pageable pageable) {
		try {
			return ResponseEntity.ok().body(service.findAllByDeletedFalse(pageable));
		} catch (InternalError e) {
			log.error("InternalError {}", e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		} catch (Exception e1) {
			log.error("Exception {}", e1);
			return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).build();
		}
	}

	@PostMapping("store")
	public ResponseEntity<?> store(@Valid @RequestBody Warehouse warehouse, BindingResult result) {
		try {
			if (result.hasErrors()) {
				return validation(result);
			}

			return ResponseEntity.status(HttpStatus.CREATED).body(service.save(warehouse));
		} catch (InternalError e) {
			log.error("InternalError {}", e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		} catch (Exception e1) {
			log.error("Exception {}", e1);
			return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).build();
		}
	}

	@GetMapping("/get/{id}")
	public ResponseEntity<?> get(@PathVariable Long id) {
		try {
			Warehouse warehouse = service.findById(id);

			if (warehouse == null) {
				return ResponseEntity.notFound().build();
			}
			
			Set<ProductWarehouse> sList = warehouse.getProducts().stream().filter(pW -> !pW.getDeleted())
					.collect(Collectors.toSet());

			warehouse.setProducts(sList);

			return ResponseEntity.ok().body(warehouse);
		} catch (InternalError e) {
			log.error("InternalError {}", e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		} catch (Exception e1) {
			log.error("Exception {}", e1);
			return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).build();
		}
	}

	@PutMapping("update/{id}")
	public ResponseEntity<?> update(@Valid @RequestBody Warehouse warehouse, BindingResult result,
			@PathVariable Long id) {
		try {
			if (result.hasErrors()) {
				return validation(result);
			}

			Warehouse warehouseToUpdate = service.findById(id);

			if (warehouseToUpdate == null) {
				return ResponseEntity.notFound().build();
			}

			warehouseToUpdate.setName(warehouse.getName());
			warehouseToUpdate.setWarehouseIdentifier(warehouse.getWarehouseIdentifier());
			warehouseToUpdate.setAddress(warehouse.getAddress());

			return ResponseEntity.status(HttpStatus.CREATED).body(service.save(warehouseToUpdate));
		} catch (InternalError e) {
			log.error("InternalError {}", e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		} catch (Exception e1) {
			log.error("Exception {}", e1);
			return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).build();
		}
	}

	@DeleteMapping("delete/{id}")
	public ResponseEntity<?> delete(@PathVariable Long id) {
		try {
			return service.deleteById(id) ? ResponseEntity.noContent().build() : ResponseEntity.notFound().build();
		} catch (InternalError e) {
			log.error("InternalError {}", e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		} catch (Exception e1) {
			log.error("Exception {}", e1);
			return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).build();
		}
	}

	@PutMapping("/stock-management/{id}")
	public ResponseEntity<?> stockManagement(@Valid @RequestBody Warehouse warehouse, BindingResult result,
			@PathVariable Long id) {

		try {
			if (result.hasErrors()) {
				return validation(result);
			}

			Warehouse warehouseToUpdate = service.findById(id);

			if (warehouseToUpdate == null) {
				return ResponseEntity.notFound().build();
			}

			warehouseToUpdate.getProducts().forEach(wP1 -> {
				warehouse.getProducts().forEach(wP2 -> {
					if (wP1.getId() == wP2.getId()) {
						wP1.setQuantity(wP2.getQuantity());
					}
				});
			});

			return ResponseEntity.status(HttpStatus.CREATED).body(service.save(warehouseToUpdate));
		} catch (InternalError e) {
			log.error("InternalError {}", e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		} catch (Exception e1) {
			log.error("Exception {}", e1);
			return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).build();
		}
	}

	@DeleteMapping("product-delete-from-stock/{warehouseId}/{pwId}")
	public ResponseEntity<?> productDeleteFromStock(@PathVariable Long warehouseId, @PathVariable Long pwId) {
		try {
			Warehouse warehouseToUpdate = service.findById(warehouseId);
			
			if (warehouseToUpdate == null) {
				return ResponseEntity.notFound().build();
			} else {
				boolean found = false;
				
				for(ProductWarehouse p : warehouseToUpdate.getProducts()) {
					if (p.getId() == pwId) {
						found = true;
						break;
					}
				}
				
				if (!found) {
					return ResponseEntity.notFound().build();
				}
			}

			warehouseToUpdate.removeProduct(pwId);
			
			this.service.save(warehouseToUpdate);
			
			Set<ProductWarehouse> list = warehouseToUpdate.getProducts().stream()
					.filter(wP -> !wP.getDeleted()).collect(Collectors.toSet());
			
			warehouseToUpdate.setProducts(list);
			
			return ResponseEntity.status(HttpStatus.CREATED).body(warehouseToUpdate);
		} catch (InternalError e) {
			log.error("InternalError {}", e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		} catch (Exception e1) {
			log.error("Exception {}", e1);
			return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).build();
		}
	}

	@GetMapping("/available-products-not-assigned/{id}")
	public ResponseEntity<?> availableProductsNotAssigned(@PathVariable Long id) {
		try {
			Warehouse warehouse = service.findById(id);
			
			if (warehouse == null) {
				return ResponseEntity.notFound().build();
			}
			
			List<Product> allProductsList = service.getEnableProducts();
			
			if (warehouse.getProducts().size() >= 0) {
				allProductsList = allProductsList.stream()
						.filter(product -> {
							Boolean found = true;
							
							for (ProductWarehouse pw : warehouse.getProducts()) {
								if (pw.getProduct().getId() == product.getId()) {
									if (!pw.getDeleted()) {
										found = false;	
									} 
								}
							}
							
							return found;
						})
						.collect(Collectors.toList());		
			}

			return ResponseEntity.ok().body(allProductsList);
		} catch (InternalError e) {
			log.error("InternalError {}", e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		} catch (Exception e1) {
			log.error("Exception {}", e1);
			return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).build();
		}
	}
	
	@PutMapping("add-product-to-warehouse")
	public ResponseEntity<?> addProductToWarehouse(@Valid @RequestBody ProductWarehouse productWarehouse, BindingResult result) {
		try {
			if (result.hasErrors()) {
				return validation(result);
			}
			
			Warehouse warehouse = this.service.findById(productWarehouse.getWarehouse().getId());

			if (warehouse == null) {
				return ResponseEntity.notFound().build();
			}

			boolean found = false;
			for (ProductWarehouse pw : warehouse.getProducts()) {
				if (pw.getProduct().getId() == productWarehouse.getProduct().getId()) {
					pw.setDeleted(false);
					found = true;
					break;
				}
			}
			
			if (!found) {
				ProductWarehouse dbPW = new ProductWarehouse();
				dbPW.setProduct(productWarehouse.getProduct());
				dbPW.setWarehouse(productWarehouse.getWarehouse());
				dbPW.setQuantity(productWarehouse.getQuantity());
				
				warehouse.addProduct(dbPW);
			} 

			return ResponseEntity.status(HttpStatus.CREATED).body(this.service.save(warehouse));
		} catch (InternalError e) {
			log.error("InternalError {}", e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		} catch (Exception e1) {
			log.error("Exception {}", e1);
			return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).build();
		}
	}
	
	@GetMapping("/get-products-ids-by-warehouse/{id}")
	public ResponseEntity<?> getProductsIdsByWarehouse(@PathVariable Long id) {
		try {
			Warehouse warehouse = service.findById(id);

			if (warehouse == null) {
				return ResponseEntity.notFound().build();
			}
			
			Set<Long> productsIds = warehouse.getProducts().stream()
					.filter(pW -> !pW.getDeleted())
					.map(pW -> pW.getProduct().getId())
					.collect(Collectors.toSet());
			
			return ResponseEntity.ok().body(productsIds);
		} catch (InternalError e) {
			log.error("InternalError {}", e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		} catch (Exception e1) {
			log.error("Exception {}", e1);
			return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).build();
		}
	}
	
	@GetMapping("/get-stock-by-warehouse-and-product/{warehouseId}/{productId}")
	public ResponseEntity<?> getStockByWarehouseAndProduct(@PathVariable Long warehouseId, @PathVariable Long productId) {
		try {
			Warehouse warehouse = service.findById(warehouseId);

			if (warehouse == null) {
				return ResponseEntity.notFound().build();
			}
			
			Optional<ProductWarehouse> stock = warehouse.getProducts().stream().
				    filter(p -> p.getProduct().getId() == productId).
				    findFirst();
			
			if (stock.isEmpty()) {
				return ResponseEntity.notFound().build();
			}

			return ResponseEntity.ok().body(stock.get().getQuantity());
		} catch (InternalError e) {
			log.error("InternalError {}", e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		} catch (Exception e1) {
			log.error("Exception {}", e1);
			return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).build();
		}
	}
	
	@PutMapping("/sotck-update-after-order-placed/{id}")
	public ResponseEntity<?> sotckUpdateAfterOrderPlaced(@RequestBody List<Map<String, Long>> stock, @PathVariable Long id) {
		try {

			Warehouse warehouseToUpdate = service.findById(id);

			if (warehouseToUpdate == null) {
				return ResponseEntity.notFound().build();
			}

			Map<String, String> errors = stockUpdateCheck(warehouseToUpdate, stock);
			
			if (errors.size() > 0) {
				return ResponseEntity.badRequest().body(errors);
			}
			
			warehouseToUpdate.getProducts().stream()
					.forEach(pw -> {
						stock.stream()
						.forEach(s -> {
							if (pw.getProduct().getId() == s.get("productId")) {
								service.stockUpdateAfterOrderPlaced(pw.getId(), pw.getQuantity() - s.get("quantity"));
							}
							
						});
					});
			
			Map<String, Boolean> result = new HashMap<>();
			result.put("result", true);
			
			return ResponseEntity.status(HttpStatus.CREATED).body(result);
		} catch (InternalError e) {
			log.error("InternalError {}", e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		} catch (Exception e1) {
			log.error("Exception {}", e1);
			return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).build();
		}
	}
	
	private ResponseEntity<?> validation(BindingResult result) {
		Map<String, Object> errors = new HashMap<>();

		result.getFieldErrors().forEach(err -> {
			errors.put(err.getField(), "El campo " + err.getField() + " " + err.getDefaultMessage());
		});

		return ResponseEntity.badRequest().body(errors);
	}

	private Map<String, String> stockUpdateCheck(Warehouse warehouse, List<Map<String, Long>> stock) {
		Map<String, String> errors = new HashMap<>();

		warehouse.getProducts().stream().forEach(pw -> {	
			stock.stream().forEach(s -> {
				if (pw.getProduct().getId() == s.get("productId")) {
					if (pw.getQuantity() < s.get("quantity")) {
						String product = "(".concat(pw.getProduct().getSku().concat(") - ".concat(pw.getProduct().getName())));
						errors.put("error" + errors.size(), ("The quantity requested cannot be processed for: ".concat(product)));
					}
				}
			});	
		});
		
		return errors;
	}
}
