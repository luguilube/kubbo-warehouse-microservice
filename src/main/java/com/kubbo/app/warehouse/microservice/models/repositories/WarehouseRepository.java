package com.kubbo.app.warehouse.microservice.models.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.kubbo.app.warehouse.microservice.models.entities.Warehouse;

public interface WarehouseRepository extends PagingAndSortingRepository<Warehouse, Long> {
	public List<Warehouse> findAllByDeletedFalse();
	public Page<Warehouse> findAllByDeletedFalse(Pageable pageable);
	public Optional<Warehouse> findById(Long id);
	
	@Modifying(clearAutomatically = true)
	@Query("update ProductWarehouse pw set pw.quantity = ?2 where pw.id = ?1")
	public void stockUpdateAfterOrderPlaced(Long id, Long quantity);
}
