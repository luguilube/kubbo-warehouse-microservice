package com.kubbo.app.warehouse.microservice.models.entities;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotEmpty;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "warehouses")
public class Warehouse {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String name;
	
	@NotEmpty
	private String warehouseIdentifier;
	
	@NotEmpty
	private String address;
	
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern="dd/MM/yyyy")
	private Date createdAt;
	
	@Column(columnDefinition = "boolean default false")
	private Boolean deleted;
	
	@JsonIgnoreProperties(value = {"warehouse", "hibernateLazyInitializer", "handler"}, allowSetters = true)
	@OneToMany(mappedBy = "warehouse", fetch=FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<ProductWarehouse> products;
	
	public Warehouse() {
		products = new HashSet<>();
	}
	
	@PrePersist
	private void prePersist() {
		createdAt = new Date();
		deleted = deleted == null ? false : deleted;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getWarehouseIdentifier() {
		return warehouseIdentifier;
	}

	public void setWarehouseIdentifier(String warehouseIdentifier) {
		this.warehouseIdentifier = warehouseIdentifier;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Boolean getDeleted() {
		return deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}
	
	public Set<ProductWarehouse> getProducts() {
		return products;
	}

	public void setProducts(Set<ProductWarehouse> products) {
		this.products = products;
	}
	
	public void addProduct(ProductWarehouse product) {
		this.products.add(product);
	}
	
	public void removeProduct(Long id) {
		this.products.forEach(wP1 -> {
			if (wP1.getId() == id) {
				wP1.setDeleted(true);
			}
		});
	}

	@Override
    public String toString() {
        return "Warehouse{" +
                "id=" + id +
                ", name='" + name + "'" +
                ", warehouseIdentifier='" + warehouseIdentifier + "'" +
                ", address='" + address + "'" +
                ", createdAt='" + createdAt + "'" +
                ", deleted=" + deleted + 
                '}';
    }
}
