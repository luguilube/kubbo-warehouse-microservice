package com.kubbo.app.warehouse.microservice.models.entities;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotEmpty;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.sun.istack.NotNull;

@Entity
@Table(name = "products")
public class Product {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@NotEmpty
	private String sku;
	
	@NotEmpty
	private String name;
	
	private String description;
	
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern="dd/MM/yyyy")
	private Date createdAt;
	
	@NotNull
	private Boolean enabled;

	@Column(columnDefinition = "boolean default false")
	private Boolean deleted;
	
	@JsonIgnoreProperties(value = {"product", "hibernateLazyInitializer", "handler"}, allowSetters = true)
	@OneToMany(mappedBy = "warehouse", fetch=FetchType.LAZY)
    private Set<ProductWarehouse> warehouses;
	
	public Product() {
		warehouses = new HashSet<ProductWarehouse>();
	}
	
	@PrePersist
	private void prePersist() {
		createdAt = new Date();
		deleted = deleted == null ? false : deleted;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSku() {
		return sku;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}
	
	public Boolean getDeleted() {
		return deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	public Set<ProductWarehouse> getWarehouses() {
		return warehouses;
	}

	public void setWarehouse(Set<ProductWarehouse> warehouses) {
		this.warehouses = warehouses;
	}
	
	public void addWarehouse(ProductWarehouse warehouse) {
		this.warehouses.add(warehouse);
	}
	
	public void removeWarehouse(ProductWarehouse warehouse) {
		this.warehouses.remove(warehouse);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		
		if (!(obj instanceof Product)) {
			return false;
		}
		
		Product a = (Product) obj;
		
		return this.id != null && this.id.equals(a.getId());
	}
	
	@Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", sku='" + sku + "'" +
                ", name='" + name + "'" +
                ", description='" + description + "'" +
                ", createdAt='" + createdAt + "'" +
                ", enabled=" + enabled + 
                ", deleted=" + deleted + 
                '}';
    }
}
