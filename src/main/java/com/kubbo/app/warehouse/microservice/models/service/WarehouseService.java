package com.kubbo.app.warehouse.microservice.models.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.kubbo.app.warehouse.microservice.models.entities.Product;
import com.kubbo.app.warehouse.microservice.models.entities.Warehouse;

public interface WarehouseService {
	public List<Warehouse> findAllByDeletedFalse();
	public Page<Warehouse> findAllByDeletedFalse(Pageable pageable);
	public Warehouse save(Warehouse warehouse);
	public Warehouse findById(Long id);
	public boolean deleteById(Long id);
	public List<Product> getEnableProducts();
	public Product getProduct(Long id);
	public void stockUpdateAfterOrderPlaced(Long id, Long quantity);
}
