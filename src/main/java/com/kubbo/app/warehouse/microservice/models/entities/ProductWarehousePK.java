package com.kubbo.app.warehouse.microservice.models.entities;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class ProductWarehousePK implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long productId;
	private Long warehouseId;
	
	public Long getProductId() {
		return productId;
	}
	public void setProductId(Long productId) {
		this.productId = productId;
	}
	public Long getWarehouseId() {
		return warehouseId;
	}
	public void setWarehouseId(Long warehouseId) {
		this.warehouseId = warehouseId;
	}
}
