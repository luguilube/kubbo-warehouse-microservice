package com.kubbo.app.warehouse.microservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableFeignClients
@EnableEurekaClient
@SpringBootApplication
public class KubboWarehouseMicroserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(KubboWarehouseMicroserviceApplication.class, args);
	}

}
