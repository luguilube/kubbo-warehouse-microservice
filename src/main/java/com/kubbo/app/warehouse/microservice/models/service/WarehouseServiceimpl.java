package com.kubbo.app.warehouse.microservice.models.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.kubbo.app.warehouse.microservice.clients.ProductFeignClient;
import com.kubbo.app.warehouse.microservice.models.entities.Product;
import com.kubbo.app.warehouse.microservice.models.entities.Warehouse;
import com.kubbo.app.warehouse.microservice.models.repositories.WarehouseRepository;

@Service
public class WarehouseServiceimpl implements WarehouseService{
	
	@Autowired
	private WarehouseRepository repository;

	@Autowired
	private ProductFeignClient productFeignClient;
	
	@Override
	@Transactional(readOnly = true)
	public List<Warehouse> findAllByDeletedFalse() {
		return repository.findAllByDeletedFalse();
	}

	@Override
	@Transactional(readOnly = true)
	public Page<Warehouse> findAllByDeletedFalse(Pageable pageable) {
		return repository.findAllByDeletedFalse(pageable);
	}

	@Override
	@Transactional
	public Warehouse save(Warehouse warehouse) {
		return repository.save(warehouse);
	}

	@Override
	@Transactional(readOnly = true)
	public Warehouse findById(Long id) {
		return repository.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public boolean deleteById(Long id) {
		Optional<Warehouse> op = repository.findById(id);
		
		if (!op.isEmpty()) {
			op.get().setDeleted(true);
			op.get().getProducts().forEach(product -> {
				product.setDeleted(true);
			});
			
			repository.save(op.get());
			
			return true;
		}
		
		return false;	
	}

	@Override
	public List<Product> getEnableProducts() {
		return productFeignClient.getEnableProducts();
	}

	@Override
	public Product getProduct(Long id) {
		return productFeignClient.get(id);
	}

	@Override
	@Transactional
	public void stockUpdateAfterOrderPlaced(Long id, Long quantity) {
		repository.stockUpdateAfterOrderPlaced(id, quantity);
	}

	

}
