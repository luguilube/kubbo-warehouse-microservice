package com.kubbo.app.warehouse.microservice.clients;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.kubbo.app.warehouse.microservice.models.entities.Product;

@FeignClient(name = "kubbo-product-microservice")
public interface ProductFeignClient {
	@GetMapping("/get-enable-products")
	public List<Product> getEnableProducts();
	
	@GetMapping("/get/{id}")
	public Product get(@PathVariable Long id);
}
