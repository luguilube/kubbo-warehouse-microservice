FROM openjdk:11

VOLUME /tmp

ADD ./target/kubbo-warehouse-microservice-0.0.1-SNAPSHOT.jar kubbo-warehouse-microservice.jar

ENTRYPOINT ["java", "-jar", "/kubbo-warehouse-microservice.jar"]